#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "arvore_b.h"

arvore_b inicializaArvoreB(){
    return NULL;
}

tipo_registro* pesquisa(apontador p, tipo_chave ch){
    int i = 1;
    if (p == NULL){
        printf("tipo_registro nao esta presente na arvore\n");
        return NULL;
    }
    while (i < p->n && ch.chave > p->r[i-1].chave)
        i++;
    if (ch.chave == p->r[i-1].chave) {
        return &(p->r[i-1]);
    }
    if (ch.chave < p->r[i-1].chave)
        return pesquisa(p->p[i-1], ch);
    else
        return pesquisa(p->p[i], ch);
}

void insere_na_pagina(apontador p, tipo_registro reg, apontador pDir){
    short naoAchouPosicao;
    int k;
    k = p->n;
    naoAchouPosicao = (k > 0);
    while (naoAchouPosicao){
        if (reg.chave >= p->r[k-1].chave){
            naoAchouPosicao = 0;
            break;
        }
        p->r[k] = p->r[k-1];
        p->p[k+1] = p->p[k];
        k--;
        if (k < 1)
            naoAchouPosicao = 0;
    }
    p->r[k] = reg;
    p->p[k+1] = pDir;
    p->n++;
}

void ins(tipo_registro* reg, apontador p, short *cresceu, tipo_registro *regRetorno,  apontador *pRetorno, int id_arquivo){
    int i = 1, j;
    apontador pTemp;
    tipo_elemento_lista tel;

    if (p == NULL){
        *cresceu = TRUE;
        (*regRetorno) = *reg;
        (*pRetorno) = NULL;
        //reg->li = cria_lista();
        tel.frequencia = 1;
        tel.indice_arquivo = id_arquivo; //sempre sera id == 1
        insere_lista(reg->li, tel); //nodo aponta para o primeiro elemento da lista
        return;
    }
    while (i < p->n && (strncmp(reg->chave, p->r[i-1].chave, N_COMPARAR) > 0))
        i++;

    if (!strncmp(reg->chave, p->r[i-1].chave, N_COMPARAR)){
        //printf(" Erro: registro ja esta presente\n");
        tipo_nodo_lista* tnl = pesquisa_lista_id(p->r[i-1].li, id_arquivo);
        if (tnl != NULL){ //ja tem esse ID na lista
            tnl->info.frequencia++;
        }
        else{ //nao tem esse ID, insere ID
            tel.frequencia = 1;
            tel.indice_arquivo = id_arquivo;
            insere_lista(p->r[i-1].li, tel);
            aumentaConsumoMemoriaB(sizeof(tipo_nodo_lista));
        }
        *cresceu = FALSE;
        return;
    }
    if (strncmp(reg->chave, p->r[i-1].chave, N_COMPARAR) < 0)
        i--;
    ins(reg, p->p[i], cresceu, regRetorno, pRetorno, id_arquivo);

    if (!*cresceu)
        return;
    if (p->n < MM)   // Pagina tem espaco
    {
        insere_na_pagina(p, *regRetorno, *pRetorno);
        *cresceu = FALSE;
        return;
    }
    // Overflow: Pagina tem que ser dividida
    pTemp = (apontador)malloc(sizeof(tipo_pagina));
    aumentaConsumoMemoriaB(sizeof(tipo_pagina));
    pTemp->n = 0;
    pTemp->p[0] = NULL;
    if (i < M_B + 1) {
        insere_na_pagina(pTemp, p->r[MM-1], p->p[MM]);
        p->n--;
        insere_na_pagina(p, *regRetorno, *pRetorno);
    }
    else
        insere_na_pagina(pTemp, *regRetorno, *pRetorno);
    for (j = M_B + 2; j <= MM; j++)
        insere_na_pagina(pTemp, p->r[j-1], p->p[j]);
    p->n = M_B;
    pTemp->p[0] = p->p[M_B+1];
    *regRetorno = p->r[M_B];
    *pRetorno = pTemp;
}

void insere(tipo_chave_tabela chave, apontador *p, int indiceArquivo){
    short cresceu;
    tipo_registro reg;
    memset(reg.chave, '\0', N);
    strncpy(reg.chave, chave.palavra, N_COMPARAR);
    reg.li = cria_lista();

    tipo_registro regRetorno;
    tipo_pagina *pRetorno, *pTemp;
    ins(&reg, *p, &cresceu, &regRetorno, &pRetorno, indiceArquivo);
    if (cresceu){  // Arvore cresce na altura pela raiz
        pTemp = (tipo_pagina *)malloc(sizeof(tipo_pagina));
        pTemp->n = 1;
        pTemp->r[0] = regRetorno;
        pTemp->p[1] = pRetorno;
        pTemp->p[0] = *p;
        *p = pTemp;
        aumentaConsumoMemoriaB(sizeof(tipo_pagina));
    }
}


int main(){
    printf("ARVORE B\n");
}