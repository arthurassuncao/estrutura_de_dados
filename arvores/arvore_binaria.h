
//Estrutura para arvore binaria
typedef int tipo_chave;
typedef struct
{
    tipo_chave chave;
    //Demais componentes
}tipo_elemento;

typedef struct nodo
{
    tipo_elemento info;
    struct nodo *esq, *dir;
}tipo_nodo;

typedef tipo_nodo *apontador;

//Funcoes

apontador inicializa();

apontador pesquisa(apontador p, tipo_chave ch); //pesquisa um elemento na arvore

apontador pesquisaSemRecursao(apontador p, tipo_chave ch);

int insere(apontador *p, tipo_elemento e);

int insereSemRecursao(apontador *p, tipo_elemento e);

void in_ordem(apontador p); //Esquerdo Pai Direito

void pre_ordem(apontador p); //Pai Esquerdo Direito

void pos_ordem(apontador p); //Esquerdo Direito Pai

int remover(apontador *p, tipo_chave ch); //remove um elemento

int removerSemRecursao(apontador *p, tipo_chave ch);

int verificaArvore(apontador p); //verifica se a arvore eh uma arvore binaria de pesquisa

int alturaArvore(apontador p);
