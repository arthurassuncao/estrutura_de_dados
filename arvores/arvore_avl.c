#include <stdio.h>
#include <stdlib.h>
#include "arvore_avl.h"

apontador inicializa()
{
    return NULL;
}

apontador pesquisa(apontador p, tipo_chave ch)
{
    if (!p)
        return NULL;
    //else
    if (p->info.chave == ch)
        return p;
    //else
    if (p->info.chave > ch)
        return pesquisa(p->esq, ch);
    //else
    return pesquisa(p->dir, ch);
}

apontador pesquisaSemRecursao(apontador p, tipo_chave ch)
{
    while(p)
    {
        if (p->info.chave == ch)
            return p;
        //else
        if (p->info.chave > ch)
            p = p->esq;
        else
            p = p->dir;
    }
    return NULL;
}

int insere(apontador *p, tipo_elemento e)
{
    int aux;
    if (!(*p)){
        *p = (apontador)malloc(sizeof(tipo_nodo));
        (*p)->info = e;
        (*p)->h = 0;
        (*p)->esq = NULL;
        (*p)->dir = NULL;
        return 1;
    }
    //else
    if ((*p)->info.chave == e.chave)
        return 0;
    //else
    if ((*p)->info.chave > e.chave){
        aux = insere(&(*p)->esq, e);
        if(aux == 1)
            (*p)->h = alturaArvore((*p)->dir) - alturaArvore((*p)->esq);
        if((*p)->h == -2){
            if((*p)->esq->h == -1){
                rotaciona_direita(&(*p));
            }
            else if((*p)->esq->h == 1){
                rotaciona_esquerda(&(*p)->esq);
                rotaciona_direita(&(*p));
            }
        }
        return aux;
    }
    //else
    aux = insere(&(*p)->dir, e);
    if(aux == 1)
        (*p)->h = alturaArvore((*p)->dir) - alturaArvore((*p)->esq);
    if((*p)->h == 2){
        if((*p)->dir->h == 1){
            rotaciona_esquerda(&(*p));
        }
        else if((*p)->dir->h == -1){
            rotaciona_direita(&(*p)->dir);
            rotaciona_esquerda(&(*p));
        }
    }
    return aux;
}

//nao ta avl
int insereSemRecursao(apontador *p, tipo_elemento e)
{
    //apontador *a;
    //a=p;

    while(*p)
    {
        if ((*p)->info.chave == e.chave)
            return 0;
        //else
            if ((*p)->info.chave > e.chave)
                p = &(*p)->esq;
            else
                p = &(*p)->dir;
    }
    *p = (apontador)malloc(sizeof(tipo_nodo));
    (*p)->info = e;
    (*p)->esq = NULL;
    (*p)->dir = NULL;
    return 1;
}

void in_ordem(apontador p) //Esquerdo Pai Direito
{
    if (!p)
        return;
    in_ordem(p->esq);
    //processa nodo
    printf("%d(%d) ",p->info.chave, p->h);
    in_ordem(p->dir);

}

void pre_ordem(apontador p) //Pai Esquerdo Direito
{
    if(!p)
        return;
    printf("%d(%d) ", p->info.chave, p->h);
    pre_ordem(p->esq);
    pre_ordem(p->dir);
}

void pos_ordem(apontador p) //Esquerdo Direito Pai
{
    if(!p)
        return;
    pos_ordem(p->esq);
    pos_ordem(p->dir);
    printf("%d(%d) ", p->info.chave, p->h);
}

int remover(apontador *p, tipo_chave ch)
{
    int aux;
    tipo_nodo *a;
    if (!(*p))
        return 0;
    else
        if ((*p)->info.chave > ch){
            aux = remover(&(*p)->esq, ch);
            if(aux == 1){
                (*p)->h = alturaArvore((*p)->dir) - alturaArvore((*p)->esq);
                if((*p)->h == 2){ //desbalanceou pra direita
                    if((*p)->dir->h == 1){
                        rotaciona_esquerda(&(*p));
                    }
                    else if((*p)->dir->h == -1){
                        rotaciona_direita(&(*p)->dir);
                        rotaciona_esquerda(&(*p));
                    }
                }
            }
            return aux;
        }
        else
        if ((*p)->info.chave < ch){
            aux = remover(&(*p)->dir, ch);
            if(aux == 1){
                (*p)->h = alturaArvore((*p)->dir) - alturaArvore((*p)->esq);
                if((*p)->h == -2){
                    if((*p)->esq->h == -1){
                        rotaciona_direita(&(*p));
                    }
                    else if((*p)->esq->h == 1){
                        rotaciona_esquerda(&(*p)->esq);
                        rotaciona_direita(&(*p));
                    }
                }
            }
            return aux;
        }
        else
        {
            a = *p;
            if (!((*p)->dir)) //nao possui filho direito
                *p = (*p)->esq;
            else if(!((*p)->esq))
                    *p = (*p)->dir;
            else
            {
                p = &(*p)->esq;
                while((*p)->dir) //enquanto tiver um filho direito
                    p = &(*p)->dir;
                a->info = (*p)->info;
                a = *p;
                *p = (*p)->esq;
            }
            free(a);
            return 1;
        }
    return 1;
}

//nao ta avl
int removerSemRecursao(apontador *p, tipo_chave ch)
{
    tipo_nodo *a;

    while (*p)
    {
        if ((*p)->info.chave == ch)
            break;
        else
            if ((*p)->info.chave > ch)
                p = &(*p)->esq;
            else
                p = &(*p)->dir;
    }
    if (!(*p))
        return 0;
    a = *p;
    if (!((*p)->dir)) //nao possui filho direito
        *p = (*p)->esq;
    else
        if(!((*p)->esq))
            *p = (*p)->dir;
        else
        {
            p = &(*p)->esq;
            while((*p)->dir) //enquanto tiver um filho direito
                p = &(*p)->dir;
            a->info = (*p)->info;
            a = *p;
            *p = (*p)->esq;
        }
    free(a);
    return 1;
}

int verificaArvore(apontador p) //verifica se a arvore eh uma arvore binaria de pesquisa
{
    if (!p)
        return 1;
    if (p->esq)
        if (p->info.chave < p->esq->info.chave)
            return 0;
    if (p->dir)
        if (p->info.chave > p->dir->info.chave)
            return 0;
    if (verificaArvore(p->esq) == 1)
        return verificaArvore(p->dir);
    else
        return 0;
}

int alturaArvore(apontador p)
{
    int ae, ad; //altura esq, altura dir
    if (!p)
        return -1;
    else{
        ae = alturaArvore(p->esq);
        ad = alturaArvore(p->dir);
        if (ae > ad)
            return ae+1;
        else
            return ad+1;
    }
}

int verificaArvoreEstritamenteBinaria(apontador p) //verifica se a arvore eh estritamente binaria, ou seja, todos os nodos internos tem todos os filhos(2) ou nenhum
{
    if (!p)
        return 1;
    if (p->esq && p->dir)
    {
        if(verificaArvoreEstritamenteBinaria(p->esq) == 1)
            return verificaArvoreEstritamenteBinaria(p->dir);
        else
            return 0;
    }
    else
        if (!p->esq && !p->dir)
            return 1;
        else
            return 0;
}

// LL
void rotaciona_direita(apontador *p){
    tipo_nodo *a;
    a = (*p)->esq;
    (*p)->esq = a->dir;
    a->dir = *p;
    (*p)->h = 0;
    a->h = 0;
    *p = a;
}

// RR
void rotaciona_esquerda(apontador *p){
    tipo_nodo *a;
    a = (*p)->dir;
    (*p)->dir = a->esq;
    a->esq = *p;
    (*p)->h = 0;
    a->h = 0;
    *p = a;
}

int main()
{
    apontador raiz;
    tipo_elemento e;

    printf("ARVORE AVL\n");

    raiz = inicializa();

    e.chave = 1;
    insere(&raiz, e);

    e.chave = 0;
    insere(&raiz, e);

    e.chave = 2;
    insere(&raiz, e);

    e.chave = 3;
    insere(&raiz, e);

    /*e.chave = 10;
    insere(&raiz, e);

    e.chave = 9;
    insere(&raiz, e);*/

    in_ordem(raiz);
    printf("\n\n");

    //remover(&raiz, 300);
    remover(&raiz, 0);

    printf("In Ordem: ");
    in_ordem(raiz);
    printf("\n\n");

    printf("E arvore binaria? %d",verificaArvore(raiz));

    printf("\n\n");

    printf("Pre Ordem: ");
    pre_ordem(raiz);
    printf("\n\n");

    printf("Pos Ordem: ");
    pos_ordem(raiz);
    printf("\n\n");


    printf("\n\n");
    return 0;
}