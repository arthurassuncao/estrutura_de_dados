#include <stdio.h>
#include <stdlib.h>
#include "arvore_binaria.h"

int main()
{
    apontador raiz;
    tipo_elemento e;

    raiz = inicializa();

    e.chave = 100;
    insere(&raiz, e);

    e.chave = 80;
    insere(&raiz, e);

    e.chave = 40;
    insere(&raiz, e);

    e.chave = 50;
    insere(&raiz, e);

    e.chave = 60;
    insere(&raiz, e);

    e.chave = 55;
    insere(&raiz, e);

    e.chave = 90;
    insere(&raiz, e);

    e.chave = 200;
    insere(&raiz, e);

    e.chave = 150;
    insere(&raiz, e);

    e.chave = 300;
    insere(&raiz, e);

    e.chave = 310;
    insere(&raiz, e);

    in_ordem(raiz);
    printf("\n\n");

    //remover(&raiz, 300);
    //remover(&raiz, 80);

    printf("In Ordem: ");
    in_ordem(raiz);
    printf("\n\n");

    printf("E arvore binaria? %d",verificaArvore(raiz));

    printf("\n\n");

    printf("Pre Ordem: ");
    pre_ordem(raiz);
    printf("\n\n");

    printf("Pos Ordem: ");
    pos_ordem(raiz);
    printf("\n\n");


    printf("\n\n");
    return 0;
}
