#ifndef ARVORE_BINARIA_PALAVRA
#define ARVORE_BINARIA_PALAVRA
//tamanho maximo da palavra definido como 20
#define N 20
//Estrutura para arvore binaria
typedef char tipo_chave[N];
typedef struct{
    tipo_chave chave;
    //Demais componentes
}tipo_elemento;

typedef struct nodo{
    tipo_elemento info;
    struct nodo *esq, *dir;
}tipo_nodo;

typedef tipo_nodo *apontador;

//Funcoes

apontador inicializa(); //inicializa a arvore

apontador pesquisa(apontador p, tipo_chave ch); //pesquisa um elemento na arvore

int insere(apontador *p, tipo_elemento e); //insere na arvore

void in_ordem(apontador p); //Esquerdo Pai Direito

void pre_ordem(apontador p); //Pai Esquerdo Direito

void pos_ordem(apontador p); //Esquerdo Direito Pai

int remover(apontador *p, tipo_chave ch); //remove um elemento

int alturaArvore(apontador p); //retorna a altura da arvore

#endif //ARVORE_BINARIA_PALAVRA