#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arvore_binaria_palavra.h"

apontador inicializa()
{
    return NULL;
}

apontador pesquisa(apontador p, tipo_chave ch)
{
    //se p está vazio, entao nao achou e retorna NULL
    if (!p)
        return NULL;
    //se a chave pesquisada é igual a chave do nodo entao retorna o nodo
    if (strcmp(p->info.chave, ch) == 0)
        return p;
    //se a chave pesquisada é menor que a chave do nodo, entao pesquisa pela esquerda
    if (strcmp(p->info.chave, ch) > 0)
        return pesquisa(p->esq, ch);
    //senao, pesquisa pela direita
    return pesquisa(p->dir, ch);
}

int insere(apontador *p, tipo_elemento e)
{
    // se o nodo estiver vazio, aloca memoria e para o elemento
    if (!(*p)){
        *p = (apontador)malloc(sizeof(tipo_nodo));
        (*p)->info = e;
        (*p)->esq = NULL;
        (*p)->dir = NULL;
        return 1;
    }
    //se ja existe uma chave igual, entao nao insere
    if (strcmp((*p)->info.chave, e.chave) == 0)
        return 0;
    //se a chave é menor do que P, entao insere a esquerda
    if (strcmp((*p)->info.chave, e.chave) > 0)
        return insere(&(*p)->esq, e);
    //se a chave é maior do que P, entao insere a direita
    return insere(&(*p)->dir, e);
}

void in_ordem(apontador p) //Esquerdo Pai Direito
{
    //se P é nulo, entao finaliza a recursao
    if (!p)
        return;
    in_ordem(p->esq);
    //processa nodo
    printf("%s ",p->info.chave);
    in_ordem(p->dir);

}

void pre_ordem(apontador p) //Pai Esquerdo Direito
{
    if(!p)
        return;
    printf("%s ", p->info.chave);
    pre_ordem(p->esq);
    pre_ordem(p->dir);
}

void pos_ordem(apontador p) //Esquerdo Direito Pai
{
    if(!p)
        return;
    pos_ordem(p->esq);
    pos_ordem(p->dir);
    printf("%s ", p->info.chave);
}

int remover(apontador *p, tipo_chave ch)
{
    tipo_nodo *a;
    //se p é nulo, retorna 0
    if (!(*p))
        return 0;
    else{
        //se a chave é menor do que o nodo, entao remove a esquerda
        if((strcmp((*p)->info.chave, ch) > 0))
            return remover(&(*p)->esq, ch);
        //se a chave é maior do que o nodo, entao remove a direita
        else if((strcmp((*p)->info.chave, ch) < 0))
            return remover(&(*p)->dir, ch);
        else{
            //chave é igual, pode remover
            a = *p;
            if (!((*p)->dir)) //se nao possui filho direito, p recebe o filho a esquerda
                *p = (*p)->esq;
            else{
                if(!((*p)->esq)) //se nao possui filho esquerdo, p recebe o filho a direita
                    *p = (*p)->dir;
                else{
                    p = &(*p)->esq;
                    while((*p)->dir) //aponta para o filho mais a direita
                        p = &(*p)->dir;
                    //a recebe o valor do filho mais a direita, substitui o elemento removido pelo filho mais a direita dele
                    a->info = (*p)->info;
                    a = *p;
                    *p = (*p)->esq;
                }
            }
            //libera memoria do elemento removido
            free(a);
            return 1;
        }
    }
    return 0;
}

int alturaArvore(apontador p)
{
    int ae, ad; //altura esq, altura dir
    if (!p)
        return -1;
    else
        ae = alturaArvore(p->esq);
        ad = alturaArvore(p->dir);
        if (ae > ad)
            return ae+1;
        else
            return ad+1;
}

int main()
{
    apontador raiz;
    tipo_elemento e;

    printf("ARVORE BINARIA\n");

    raiz = inicializa();

    strcpy(e.chave, "arthur");
    insere(&raiz, e);

    strcpy(e.chave, "lucas");
    insere(&raiz, e);

    strcpy(e.chave, "debora");
    insere(&raiz, e);

    strcpy(e.chave, "suplaum");
    insere(&raiz, e);

    strcpy(e.chave, "charles");
    insere(&raiz, e);

    in_ordem(raiz);
    printf("\n\n");

    printf("In Ordem: ");
    in_ordem(raiz);

    printf("\n\n");

    printf("Pre Ordem: ");
    pre_ordem(raiz);
    printf("\n\n");

    printf("Pos Ordem: ");
    pos_ordem(raiz);
    printf("\n\n");


    printf("\n\n");
    return 0;
}