#ifndef ARVORE_B_H
#define ARVORE_B_H

#define M_B 2
#define MM  (M_B * 2)

typedef int tipo_chave;

typedef struct{
    tipo_chave chave;
}tipo_chave;

typedef struct tipo_registro {
  tipo_chave chave;
}tipo_registro;

typedef struct tipo_pagina* apontador;

typedef struct tipo_pagina {
  short n;
  tipo_registro r[MM];
  apontador p[MM + 1];
}tipo_pagina;


typedef apontador arvore_b;

arvore_b inicializaArvoreB();

tipo_registro* pesquisa(apontador p, tipo_chave ch);
void insere_na_pagina(apontador p, tipo_registro reg, apontador pDir);
void ins(tipo_registro* reg, apontador p, short *cresceu, tipo_registro *regRetorno,  apontador *pRetorno, int id_arquivo);
void insere(tipo_chave_tabela chave, apontador *p, int indiceArquivo);

//void ImprimePalavraArvoreB(apontador p, int nivel,arquivo *arquivos,FILE *arq,FILE *arq2);
//void ImprimeArvoreB(apontador p,arquivo *arquivos,FILE *arq,FILE *arq2);

//void pesquisaPalavraArvoreB(apontador dicionario,tipo_chave palavra[],arquivo *arquivosUsados,int *ok);

#endif // ARVORE_B_H
