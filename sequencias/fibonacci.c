#include <stdio.h>
#include <stdlib.h>

int fib(int n);
int fib_rec(int n);

int main(){
    int tam = 15;
    printf("%d\n", fib(tam));
    printf("\n");
    printf("%d\n", fib_rec(tam));
}

int fib(int n){
    int i, fib_ant = 1, fib = 1, soma;
    //printf("%d ", fib_ant);
    //printf("%d ", fib);
    for(i = 3; i <= n; i++){
        soma = fib + fib_ant;
        fib_ant = fib;
        fib = soma;
        //printf("%d ", soma);
    }
    return fib;
}

int fib_rec(int n){
    int x;
    if(n == 1 || n == 2){
        return 1;
    }
    else{
        x = fib_rec(n - 1) + fib_rec(n - 2);
        return x;
    }
}