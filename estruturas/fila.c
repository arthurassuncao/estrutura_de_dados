#include <stdio.h>
#include <stdlib.h>

typedef int tipo_item;

typedef struct nodo{
	tipo_item item;
	struct nodo *prox;
}tipo_nodo;

typedef struct{
	tipo_nodo *inicio, *fim;
	int tamanho;
}tipo_fila;

typedef tipo_fila* fila;

fila inicializa(){
	fila fila = (tipo_fila*)malloc(sizeof(tipo_fila));
	fila->tamanho = 0;
	fila->inicio = NULL;
	fila->fim = NULL;
	return fila;
}

int push(fila *f, tipo_item e){
	if((*f)->inicio == NULL){
		(*f)->inicio = (tipo_nodo*)malloc(sizeof(tipo_nodo));
		(*f)->inicio->item = e;
		(*f)->inicio->prox = NULL;
		(*f)->fim = (*f)->inicio;
	}
	else{
		(*f)->fim->prox = (tipo_nodo*)malloc(sizeof(tipo_nodo));
		(*f)->fim->prox->item = e;
		(*f)->fim->prox->prox = NULL;
		(*f)->fim = (*f)->fim->prox;
	}
	(*f)->tamanho++;
}

tipo_item* pop(fila *f){
	tipo_nodo *no;
	if((*f)->tamanho != 0){
		no = (*f)->inicio;
		(*f)->inicio = (*f)->inicio->prox;
		if((*f)->tamanho == 1){
			(*f)->fim = NULL;
		}
		(*f)->tamanho--;
		return &no->item;
	}
	return NULL;
}

void imprime(fila f){
	tipo_nodo* p;
	p = f->inicio;
	while(p){
		printf("%d ", p->item);
		p = p->prox;
	}
	printf("\n");
}

int main(){
	fila fila;
	tipo_item *e;
	fila = inicializa();

	push(&fila, 5);

	push(&fila, 2);

	push(&fila, 8);

	imprime(fila);

	e = pop(&fila);

	printf("removido: %d\n", *e);

	imprime(fila);

	e = pop(&fila);

	printf("removido: %d\n", *e);

	imprime(fila);


	return 0;
}