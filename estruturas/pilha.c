#include <stdio.h>
#include <stdlib.h>

typedef int tipo_item;

typedef struct nodo{
	tipo_item item;
	struct nodo *prox;
}tipo_nodo;

typedef struct{
	tipo_nodo *topo;
	int tamanho;
}tipo_pilha;

typedef tipo_pilha* pilha;

pilha inicializa(){
	pilha pilha = (tipo_pilha*)malloc(sizeof(tipo_pilha));
	pilha->tamanho = 0;
	pilha->topo = NULL;
	return pilha;
}

int push(pilha *p, tipo_item e){
	tipo_nodo *no;
	no = (tipo_nodo*)malloc(sizeof(tipo_nodo));
	no->item = e;
	no->prox = (*p)->topo;
	(*p)->topo = no;
	(*p)->tamanho++;
	return 1;
}

tipo_item* pop(pilha *p){
	tipo_nodo *no;
	if((*p)->topo != NULL){
		no = (*p)->topo;
		(*p)->topo = (*p)->topo->prox;
		(*p)->tamanho--;
		return &no->item;
	}
	return NULL;
}

void imprime(pilha p){
	tipo_nodo* pp;
	pp = p->topo;
	while(pp){
		printf("%d ", pp->item);
		pp = pp->prox;
	}
	printf("\n");
}

int main(){

	pilha pilha;
	tipo_item *e;
	pilha = inicializa();

	push(&pilha, 5);

	push(&pilha, 2);

	push(&pilha, 8);

	imprime(pilha);

	e = pop(&pilha);

	printf("removido: %d\n", *e);

	imprime(pilha);

	e = pop(&pilha);

	printf("removido: %d\n", *e);

	imprime(pilha);

	return 0;
}