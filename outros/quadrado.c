#include<stdio.h>
#include<stdlib.h>

int imprime_quadrado(int tam){
    int i;
    int j;
    for(i = 0; i < tam; i++){
        for(j = 0; j < tam; j++){
            printf("*");
        }
        printf("\n");
    }
}

int imprime_quadrado_recursivo(int tam, int l, int c){
    if(l > 0){
        printf("*");
        return imprime_quadrado_recursivo(tam, l-1, c);
    }
    if(c > 1){
        printf("\n");
        return imprime_quadrado_recursivo(tam, tam, c-1);
    }
}

int main(){
    int tam = 10;
    imprime_quadrado(tam);
    printf("\n\n");
    imprime_quadrado_recursivo(tam, tam, tam);
}