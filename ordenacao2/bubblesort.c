#include <stdio.h>
#include <stdlib.h>

void print_vetor(int* vetor, int tam);
int* bubblesort(int* vetor, int tam);

int main(){
    int tam = 10;
    int vetor[10] = {9,2,45,31,87,5,8,419,1,4};

    print_vetor(vetor, tam);
    printf("\n");
    print_vetor(bubblesort(vetor, tam), tam);

}

void print_vetor(int* vetor, int tam){
    int i = 0;
    for(i = 0; i < tam; i++){
        if(i < tam - 1)
            printf("%d - ", vetor[i]);
        else{
            printf("%d", vetor[i]);
        }
    }
    printf("\n");
}

int* bubblesort(int* vetor, int tam){
    int i, j, aux;
    
    for(i = tam-1; i > 0; i--){
        for(j = 0; j < i; j++){
            if(vetor[j] > vetor[j+1]){
                aux = vetor[j];
                vetor[j] = vetor[j+1];
                vetor[j+1] = aux;
            }
        }
    }

    return vetor;
}