#include <stdio.h>
#include <stdlib.h>

void print_vetor(int* vetor, int tam);
int* quicksort(int* vetor, int tam);
int* ordena(int* vetor, int esq, int dir);

int main(){
    int tam = 10;
    int vetor[10] = {9,2,45,31,87,5,8,419,1,4};

    print_vetor(vetor, tam);
    printf("\n");
    print_vetor(quicksort(vetor, tam), tam);

}

void print_vetor(int* vetor, int tam){
    int i = 0;
    for(i = 0; i < tam; i++){
        if(i < tam - 1)
            printf("%d - ", vetor[i]);
        else{
            printf("%d", vetor[i]);
        }
    }
    printf("\n");
}

int* quicksort(int* vetor, int tam){
    int esq = 0;
    int dir = tam-1;
    return ordena(vetor, esq, dir);
}

int* ordena(int* vetor, int esq, int dir){
    int pivo, aux;
    int i = esq;
    int j = dir;
    pivo = vetor[(esq + dir) / 2];

    while(i <= j){
        while(vetor[i] < pivo && i < dir){
            i++;
        }
        while(vetor[j] > pivo && j > esq){
            j--;
        }
        if(i <= j){
            aux = vetor[i];
            vetor[i] = vetor[j];
            vetor[j] = aux;
            i++;
            j--;
        }
    }
    if(j > esq){
        ordena(vetor, esq, j);
    }
    if(i < dir){
        ordena(vetor, i, dir);
    }
    return vetor;
}