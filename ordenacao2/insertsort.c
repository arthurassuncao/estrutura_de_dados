#include <stdio.h>
#include <stdlib.h>

void print_vetor(int* vetor, int tam);
int* insertsort(int* vetor, int tam);

int main(){
    int tam = 10;
    int vetor[10] = {9,2,45,31,87,5,8,419,1,4};

    print_vetor(vetor, tam);
    printf("\n");
    print_vetor(insertsort(vetor, tam), tam);

}

void print_vetor(int* vetor, int tam){
    int i = 0;
    for(i = 0; i < tam; i++){
        if(i < tam - 1)
            printf("%d - ", vetor[i]);
        else{
            printf("%d", vetor[i]);
        }
    }
    printf("\n");
}

int* insertsort(int* vetor, int tam){
    int i, j, aux;
    int x;
    for(i = 1; i < tam; i++){
        x = vetor[i];
        for(j = i-1; x < vetor[j] && j >= 0; j--){
            vetor[j+1] = vetor[j];
        }
        vetor[j+1] = x;
    }
    return vetor;
}