#include <stdio.h>
#include <stdlib.h>

void print_vetor(int* vetor, int tam);
int* shellsort(int* vetor, int tam);
int* shellsort2(int* vetor, int tam);

int main(){
    int tam = 10;
    int vetor[10] = {9,2,45,31,87,5,8,419,1,4};

    print_vetor(vetor, tam);
    printf("\n");
    //print_vetor(shellsort(vetor, tam), tam);
    print_vetor(shellsort2(vetor, tam), tam);
}

void print_vetor(int* vetor, int tam){
    int i = 0;
    for(i = 0; i < tam; i++){
        if(i < tam - 1)
            printf("%d - ", vetor[i]);
        else{
            printf("%d", vetor[i]);
        }
    }
    printf("\n");
}

int* shellsort(int* vetor, int tam){
    int i, j, aux;
    int h = 1;
    int x;

    while(h < tam){
        h = h * 3 + 1;
    }
    do{
        h /= 3;
        for(i = h + 1; i < tam; i++){
            x = vetor[i];
            for(j = i; vetor[j-h] > x; j -= h){
                vetor[j] = vetor[j - h];
                if(j <= h){
                    break;
                }
            }
            vetor[j] = x;
        }
    }while(h != 1);

    return vetor;
}

int* shellsort2(int* vetor, int tam){
    int i, j, aux, x;
    int h = 1;
    while(h < tam){
        h = h * 3 + 1;
    }
    do{
        h /= 3;
        for(i = h; i < tam; i++){
            x = vetor[i];
            for(j = i-h; x < vetor[j] && j >= 0; j -= h){
                vetor[j+h] = vetor[j];
            }
            vetor[j+h] = x;
        }
    }while(h != 1);
    return vetor;
}