#include <stdio.h>
#include <stdlib.h>

void print_vetor(int* vetor, int tam);
int* selectionsort(int* vetor, int tam);

int main(){
    int tam = 10;
    int vetor[10] = {9,2,45,31,87,5,8,419,1,4};

    print_vetor(vetor, tam);
    printf("\n");
    print_vetor(selectionsort(vetor, tam), tam);

}

void print_vetor(int* vetor, int tam){
    int i = 0;
    for(i = 0; i < tam; i++){
        if(i < tam - 1)
            printf("%d - ", vetor[i]);
        else{
            printf("%d", vetor[i]);
        }
    }
    printf("\n");
}

int* selectionsort(int* vetor, int tam){
    int i, j, aux;
    int menor;

    for(i = 0; i < tam-1; i++){
        menor = i;
        for(j = i+1; j < tam; j++){
            if(vetor[j] < vetor[menor]){
                menor = j;
            }
        }
        aux = vetor[i];
        vetor[i] = vetor[menor];
        vetor[menor] = aux;
    }

    return vetor;
}