#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "bubblesort.h"
#include "insertsort.h"
#include "selectionsort.h"
#include "quicksort.h"

int main(){
    int vetorA[5] = {1,5,2,8,3};
    int vetorB[5] = {1,5,2,8,3};
    int vetorC[5] = {1,5,2,8,3};
    int vetorD[5] = {1,5,2,8,3};
    int tam = 5;

    int *vetor_res;
    printf("BubbleSort\n");
    print_vetor(vetorA, tam);
    printf("\n");
    vetor_res = bubblesort(vetorA, tam);
    printf("\n");
    print_vetor(vetor_res, tam);
    printf("\n\n");

    printf("InsertSoft\n");
    print_vetor(vetorB, tam);
    printf("\n");
    vetor_res = insertsort(vetorB, tam);
    printf("\n");
    print_vetor(vetor_res, tam);
    printf("\n\n");

    printf("SelectionSort\n");
    print_vetor(vetorC, tam);
    printf("\n");
    vetor_res = selectionsort(vetorC, tam);
    printf("\n");
    print_vetor(vetor_res, tam);
    printf("\n\n");

    printf("QuickSort\n");
    print_vetor(vetorD, tam);
    printf("\n");
    vetor_res = quicksort(vetorD, 0, tam-1);
    printf("\n");
    print_vetor(vetor_res, tam);
    printf("\n\n");


    return 0;
}
