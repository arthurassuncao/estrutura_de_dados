#include <stdio.h>
#include <stdlib.h>
#include "util.h"

void print_vetor(int vetor[], int tam){
    int i = 0;
    for(i = 0; i < tam; i++){
        if (i < tam-1){
            printf("%d - ", vetor[i]);
        }
        else{
            printf("%d", vetor[i]);
        }
    }
    printf("\n");
}
