#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "bubblesort.h"

int* bubblesort(int vetor[], int tam){
    int i = 0;
    int j = 0;
    int aux;
    for(i = 0; i < tam-1; i++){
        for(j = 0; j < tam-i-1; j++){
            if (vetor[j] > vetor[j+1]){
                aux = vetor[j+1];
                vetor[j+1] = vetor[j];
                vetor[j] = aux;
            }
            print_vetor(vetor, tam);
        }
    }
    return vetor;
}
