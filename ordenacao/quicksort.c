#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "quicksort.h"

int* quicksort(int vetor[], int primeiro, int ultimo){
    int esquerda;
    int direita;
    int pivo;
    int aux;
    if(ultimo-primeiro+1 > 1){
        pivo = vetor[ultimo+1 / 2];
        esquerda = primeiro;
        direita = ultimo;
        while(esquerda <= direita){
            while(vetor[esquerda] < pivo){
                esquerda++;
            }
            while(vetor[direita] > pivo){
                direita--;
            }
            if (esquerda <= direita){
                if(esquerda != direita){
                    aux = vetor[esquerda];
                    vetor[esquerda] = vetor[direita];
                    vetor[direita] = aux;
                }
                esquerda++;
                direita--;
            }
        }
        //print_vetor(vetor, ultimo-primeiro+1);
        quicksort(vetor, primeiro, direita);
        quicksort(vetor, esquerda, ultimo);
    }
    return vetor;
}
