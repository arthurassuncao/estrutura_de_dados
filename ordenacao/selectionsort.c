#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "selectionsort.h"

int* selectionsort(int vetor[], int tam){
    int i = 0;
    int j = 0;
    int aux;
    int menor_pos;
    for(i = 0; i < tam; i++){
        menor_pos = i;
        for(j = i; j < tam; j++){
            if(vetor[j] < vetor[menor_pos]){
                menor_pos = j;
            }
        }
        aux = vetor[i];
        vetor[i] = vetor[menor_pos];
        vetor[menor_pos] = aux;

        print_vetor(vetor, tam);
    }
    return vetor;
}
