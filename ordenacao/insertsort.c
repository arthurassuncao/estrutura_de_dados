#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "insertsort.h"

int* insertsort(int vetor[], int tam){
    int i = 0;
    int j = 0;
    int aux;
    for(i = 0; i < tam; i++){
        for(j = tam-i-1; j > 1; j--){
            if(vetor[j] < vetor[j-1]){
                aux = vetor[j];
                vetor[j] = vetor[j-1];
                vetor[j-1] = aux;
            }
            print_vetor(vetor, tam);
        }
    }
    return vetor;
}
